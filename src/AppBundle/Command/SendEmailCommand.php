<?php
namespace AppBundle\Command;

use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SendEmailCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:send-email')
            ->setDescription('Will send a test email to the specified address')
            ->addArgument('email', InputArgument::REQUIRED, 'The email to send to')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $email = $input->getArgument('email');

        $message = Swift_Message::newInstance()
            ->setSubject('Dette er en test email')
            ->setFrom('info@ehandel9000.dk')
            ->setTo($email)
            ->setBody('Hejsa. Dette er en test email sendt fra time tracking', 'text/plain')
        ;

        $this->getContainer()->get('mailer')->send($message);
    }
}
