<?php
namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="time_punches")
 **/
class TimePunch {
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var int
     **/
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Employee", inversedBy="timePunches")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id")
     * @var Employee
     **/
    protected $employee;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     **/
    protected $timestamp;

    /**
     * @ORM\Column(name="`action`", type="string")
     * @var string
     **/
    protected $action;

    /**
     * Returns an array of available actions
     *
     * @return array
     */
    public static function getAvailableActions() {
        return [
            'checkIn', 'checkOut'
        ];
    }

    /**
     * Returns true if the action of this object equals $action
     *
     * @param $action
     * @return bool
     */
    public function isAction($action) {
        return $action == $this->action;
    }

    /**
     * @return Employee
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * @param Employee $employee
     * @return TimePunch
     */
    public function setEmployee($employee)
    {
        $this->employee = $employee;
        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return TimePunch
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @param \DateTime $timestamp
     * @return TimePunch
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
        return $this;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param string $action
     * @return TimePunch
     */
    public function setAction($action)
    {
        $this->action = $action;
        return $this;
    }
}