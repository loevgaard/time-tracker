<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Employee;
use AppBundle\Entity\TimePunch;
use AppBundle\Form\EmployeeType;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\NoResultException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Time;

class DefaultController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     * @Route("/{time_punch_id}", name="home", defaults={"time_punch_id" = ""}, requirements={"time_punch_id" = "\d+"})
     */
    public function indexAction(Request $request)
    {
        $identifier = $request->get('identifier');
        $timePunchId = $request->get('time_punch_id');

        if($timePunchId) {
            $timePunch = $this->getDoctrine()->getRepository('AppBundle:TimePunch')->find($timePunchId);
            return $this->render('AppBundle:Default:index.html.twig', [
                'timePunch' => $timePunch
            ]);
        } elseif($identifier) {
            $employee = $this->getDoctrine()->getRepository('AppBundle:Employee')->findOneBy([
                'identifier' => $identifier
            ]);

            if($employee) {
                /** @var EntityManager $em */
                $em = $this->getDoctrine()->getManager();
                $qb = $em->createQuery("select count(tp.id) as n from AppBundle:TimePunch tp where tp.employee = :e");
                $qb->setParameter('e', $employee);
                $timePunchCount = $qb->getSingleScalarResult();
                $action = 'checkIn';
                if($timePunchCount % 2 != 0) {
                    $action = 'checkOut';
                }

                $timePunch = new TimePunch();
                $timePunch->setEmployee($employee)
                    ->setTimestamp(new \DateTime())
                    ->setAction($action)
                ;

                $em->persist($timePunch);
                $em->flush();

                return new RedirectResponse($this->generateUrl('home', [
                    'time_punch_id' => $timePunch->getId()
                ]));
            } else {
                $this->addFlash(
                    'notice',
                    'Medarbejderen er ikke oprettet i timesystemet. Indtast data nedenfor.'
                );
                return new RedirectResponse($this->generateUrl('employee_create', [
                    'identifier' => $identifier
                ]));
            }
        }

        return $this->render('AppBundle:Default:index.html.twig');
    }

    /**
     * @return Response
     * @Route("/medarbejdere/aktiver/{id}", name="employee_activate")
     */
    public function employeeActivateAction($id) {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $employee = $em->getRepository('AppBundle:Employee')->find($id);
        $employee->setActive(true);
        $em->flush();

        return $this->redirectToRoute('employees');
    }

    /**
     * @return Response
     * @Route("/medarbejdere/deaktiver/{id}", name="employee_deactivate")
     */
    public function employeeDeactivateAction($id) {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $employee = $em->getRepository('AppBundle:Employee')->find($id);
        $employee->setActive(false);
        $em->flush();

        return $this->redirectToRoute('employees');
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/medarbejdere", name="employees")
     */
    public function employeesAction(Request $request) {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $criteria = $request->query->getBoolean('all') ? [] : ['active' => true];

        $employees = $em->getRepository('AppBundle:Employee')->findBy($criteria, ['firstName' => 'ASC', 'lastName' => 'ASC']);

        return $this->render('AppBundle:Default:employees.html.twig', [
            'employees' => $employees,
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/opret-medarbejder/{identifier}", name="employee_create", defaults={"identifier" = ""})
     */
    public function createEmployeeAction(Request $request) {
        $employee = new Employee();
        if($request->get('identifier')) {
            $employee->setIdentifier($request->get('identifier'));
        }
        $form = $this->createForm(EmployeeType::class, $employee);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($employee);
            $em->flush();

            return new RedirectResponse($this->generateUrl('home'));
        }
        return $this->render('AppBundle:Default:create_employee.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/medarbejdere/checked", name="employees_checked_in")
     */
    public function checkedInEmployeesAction(Request $request) {
        // @TODO This is insanely bad coded

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        /** @var TimePunch[] $timePunches */
        $timePunches = [];

        $employees = $em->getRepository('AppBundle:Employee')->findAll();
        foreach($employees as $employee) {
            $qb = $em->createQuery("select tp from AppBundle:TimePunch tp where tp.employee = :e order by tp.id desc");
            $qb->setMaxResults(1);
            $qb->setParameter('e', $employee);

            /** @var TimePunch $timePunch */
            try {
                $timePunch = $qb->getSingleResult();
            } catch(NoResultException $e) {
                continue;
            }

            if($timePunch->isAction('checkIn')) {
                $timePunches[] = $timePunch;
            }
        }

        return $this->render('AppBundle:Default:checked_in_employees.html.twig', [
            'timePunches' => $timePunches,
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/rapport", name="report")
     */
    public function reportAction(Request $request) {
        $response   = new Response();
        $email      = $request->get('email', '');
        $from       = $request->get('from');
        $to         = $request->get('to');

        if(!$email && $request->cookies->get('report_email')) {
            $email = $request->cookies->get('report_email');
        }

        if($from) {
            $from = \DateTime::createFromFormat($this->getParameter('date_format'), $from);
        } else {
            $from = new \DateTime();
            $from->sub(new \DateInterval('P30D'));
        }
        $from->setTime(0, 0, 0);

        if($to) {
            $to = \DateTime::createFromFormat($this->getParameter('date_format'), $to);
        } else {
            $to = new \DateTime();
            $to->sub(new \DateInterval('P1D'));
        }
        $to->setTime(23, 59, 59);

        $reportItems = [];

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $employees = $em->getRepository('AppBundle:Employee')->findAll();
        foreach($employees as $employee) {
            $qb = $em->createQuery("select tp from AppBundle:TimePunch tp where tp.employee = :e and tp.timestamp >= :from and tp.timestamp <= :to order by tp.id");
            $qb->setParameters([
                'e'     => $employee,
                'from'  => $from,
                'to'    => $to,
            ]);

            $secondsWorked = 0;

            /** @var TimePunch[] $timePunches */
            $timePunches = $qb->getResult();

            /** @var \DateTime $checkIn */
            $checkIn = null;
            foreach($timePunches as $timePunch) {
                if($checkIn) {
                    $secondsWorked += ($timePunch->getTimestamp()->getTimestamp() - $checkIn->getTimestamp());

                    $checkIn = null;
                } else {
                    $checkIn = $timePunch->getTimestamp();
                }
            }

            $hours = $secondsWorked / 60 / 60;

            if(!$hours) {
                continue;
            }

            $reportItems[] = [
                'employee'  => $employee,
                'hours'     => $hours,
            ];
        }

        $file = '';
        if($email) {
            // save email for later
            $response->headers->setCookie(new Cookie('report_email', $email, time() - (365 * 24 * 60 * 60)));

            /**
             * This file should be in accordance with the documentation found at
             * https://www.danlon.dk/front/internalresources/indlaes-til-danloen.pdf
             */
            $file = sys_get_temp_dir() . '/' . uniqid('report-') . '.csv';
            $fp = fopen($file, 'w');
            $vatId = '33585071';
            $salaryCategory = 1; // lønart in danish

            foreach ($reportItems as $reportItem) {
                fputcsv($fp, [
                    $vatId,
                    $reportItem['employee']->getIdentifier(),
                    $salaryCategory,
                    number_format($reportItem['hours'], 2, ',', '.'),
                    '',
                    ''
                ], ';');
            }

            fclose($fp);

            $templateEngine = $this->container->get('templating');

            $message = \Swift_Message::newInstance()
                ->attach(\Swift_Attachment::fromPath($file))
                ->setSubject('E-Handel 9000 Timeopgørelse | ' . $from->format('j/n Y') . ' - ' . $to->format('j/n Y'))
                ->setFrom('info@ehandel9000.dk')
                ->setTo($email)
                ->setBody($templateEngine->render(
                    'emails/report.html.twig'
                ), 'text/html')
            ;
            $res = $this->container
                ->get('mailer')
                ->send($message);
        }

        return $this->render('AppBundle:Default:report.html.twig', [
            'file'          => $file,
            'email'         => $email,
            'from'          => $from,
            'to'            => $to,
            'reportItems'   => $reportItems,
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/rapport/{identifier}", name="report_employee")
     */
    public function reportEmployeeAction(Request $request) {
        $identifier = $request->get('identifier');

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $employee = $em->getRepository('AppBundle:Employee')->findOneBy([
            'identifier' => $identifier
        ]);

        if($request->isMethod('POST')) {
            $datetimes = $request->get('datetimes');

            foreach($datetimes as $timePunchId => $datetime) {
                $timePunch = $em->getRepository('AppBundle:TimePunch')->find($timePunchId);
                $datetime = \DateTime::createFromFormat($this->getParameter('datetime_format'), $datetime);

                $timePunch->setTimestamp($datetime);
            }

            $em->flush();
        }

        /** @var TimePunch[] $timePunches */
        $timePunches = $this->getDoctrine()->getRepository('AppBundle:TimePunch')->findBy([
            'employee' => $employee,
        ], [
            'id' => 'desc'
        ]);
        return $this->render('AppBundle:Default:report_employee.html.twig', [
            'employee'      => $employee,
            'timePunches'   => $timePunches,
        ]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     * @Route("/slet-tidsstempel/{id}", name="delete_time_punch")
     */
    public function deleteTimePunch(Request $request) {
        $id             = $request->get('id');
        $entityManager  = $this->getDoctrine()->getManager();
        $timePunch      = $entityManager->getRepository('AppBundle:TimePunch')->find($id);

        if($timePunch) {
            $entityManager->remove($timePunch);
            $entityManager->flush();
        }

        $redirectUrl = $request->headers->get('referer');
        if(!$redirectUrl) {
            $redirectUrl = $this->generateUrl('home');
        }

        return new RedirectResponse($redirectUrl);
    }
}
